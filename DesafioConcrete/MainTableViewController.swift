//
//  MainTableViewController.swift
//  DesafioConcrete
//
//  Created by Victor Hugo Martins Lisboa on 27/11/2017.
//  Copyright © 2017 Victor Hugo Martins Lisboa. All rights reserved.
//

import UIKit
import Alamofire

struct repo {
    static var pulls = ""
    static var name = ""
    static var link = ""
}

class MainTableViewController: UITableViewController {
    
    var arr = [[String: AnyObject]]()
    
    func getAPI() {
        
        for i in 1...34 {
            Alamofire.request(URL(string: "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=\(i)")!).responseJSON { (response) in

                let result = response.result
                if let dict = result.value as? [String: AnyObject] {
                    guard let items = dict["items"] as? [[String: AnyObject]] else  {
                        return
                    }

                    for j in 0..<items.count {
                        guard let name = items[j]["name"] else {
                            return
                        }

                        guard let desc = items[j]["description"] else {
                            return
                        }

                        guard let pullsUrl = items[j]["pulls_url"] else {
                            return
                        }

                        guard let owner = items[j]["owner"] else {
                            return
                        }

                        guard let stars = items[j]["stargazers_count"] else {
                            return
                        }

                        guard let forks = items[j]["forks_count"] else {
                            return
                        }

                        self.arr.append(["name": name])
                        self.arr.append(["description": desc])
                        self.arr.append(["pulls_url": pullsUrl])
                        self.arr.append(["stargazers_count": stars])
                        self.arr.append(["forks_count": forks])
                        self.arr.append(["login": owner["login"] as AnyObject])
                        self.arr.append(["avatar_url": owner["avatar_url"] as AnyObject])

                        self.tableView.reloadData()
                    }
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        getAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arr.count/7
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MainTableViewCell
        
        // Configure the cell...
        cell.contentView.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        cell.cellView.backgroundColor = UIColor.white
        cell.cellView.layer.masksToBounds = false
        cell.cellView.layer.cornerRadius = 3.0
        cell.cellView.layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        cell.cellView.layer.shadowOpacity = 0.5
        cell.cellView.clipsToBounds = false
        
        cell.profImg.layer.cornerRadius = cell.profImg.frame.size.width/2
        cell.profImg.clipsToBounds = true
        
        if indexPath.row <= arr.count {
        cell.nameProject.text = ("\(arr[indexPath.row*7]["name"] as AnyObject)")

        cell.descProj.text = ("\(arr[indexPath.row*7+1]["description"] as AnyObject)")

        cell.numStar.text = ("\(Int((arr[indexPath.row*7+3]["stargazers_count"] as AnyObject) as! NSNumber))")

        cell.numFork.text = ("\(Int((arr[indexPath.row*7+4]["forks_count"] as AnyObject) as! NSNumber))")

        cell.userName.text = ("\(arr[indexPath.row*7+5]["login"] as AnyObject)")

        cell.profImg.sd_setImage(with: URL(string: ("\(arr[indexPath.row*7+6]["avatar_url"] as AnyObject)")), placeholderImage: #imageLiteral(resourceName: "octocat"), options: [.continueInBackground, .progressiveDownload])
        
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        cell.alpha = 0

        let transform = CATransform3DTranslate(CATransform3DIdentity, -(UIScreen.main.bounds.width), 20, 0)
        cell.layer.transform = transform

        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.layer.transform = CATransform3DIdentity
        }

    }

    @IBAction func refresh(_ sender: Any) {
        getAPI()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        let currentCell = tableView.cellForRow(at: indexPath!) as? MainTableViewCell
        let pull = "\(arr[(indexPath?.row)!*7+2]["pulls_url"] as AnyObject)"
        repo.pulls = pull.replacingOccurrences(of: "{/number}", with: "")
        repo.name = (currentCell?.nameProject.text)!
    }
    
    
}

