//
//  PullTableViewController.swift
//  DesafioConcrete
//
//  Created by Victor Hugo Martins Lisboa on 29/11/2017.
//  Copyright © 2017 Victor Hugo Martins Lisboa. All rights reserved.
//

import UIKit
import Alamofire

class PullTableViewController: UITableViewController {
    
    var arr = [[String: String]]()
    
    func getAPI() {
        
        Alamofire.request(URL(string: repo.pulls)!).responseJSON { (response) in
            let result = response.result
            
            if let array = result.value as? [[String: AnyObject]] {

                for i in 0..<array.count {

                    guard let head = array[i]["head"] as? [String: AnyObject] else {
                        return
                    }

                    guard let repo = head["repo"] as? [String: AnyObject] else {
                        return
                    }

                    guard let owner = repo["owner"] as? [String: AnyObject] else {
                        return
                    }
                    
                    self.arr.append(["title": array[i]["title"] as! String])
                    self.arr.append(["body": array[i]["body"] as? String ?? "No description"])
                    self.arr.append(["login": owner["login"] as! String])
                    self.arr.append(["avatar_url": owner["avatar_url"] as! String])
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    let date = dateFormatter.date(from: (repo["pushed_at"] as! String).replacingOccurrences(of: "Z", with: ""))
                    
                    dateFormatter.dateFormat = "dd/MM/yyy HH:mm:ss"
                    let finalDate = dateFormatter.string(from: date!)
                    
                    self.arr.append(["pushed_at": finalDate])
                    
                    self.arr.append(["html_url": array[i]["html_url"] as! String])
                    
                    self.tableView.reloadData()
                    
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = repo.name
        tableView.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        getAPI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arr.count/6
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PullTableViewCell

        cell.contentView.backgroundColor = UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        cell.cellView.backgroundColor = UIColor.white
        cell.cellView.layer.masksToBounds = false
        cell.cellView.layer.cornerRadius = 3.0
        cell.cellView.layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        cell.cellView.layer.shadowOpacity = 0.5
        cell.cellView.clipsToBounds = false
        
        
        cell.profImg.layer.borderWidth = 2.0
        cell.profImg.layer.borderColor = UIColor.lightGray.cgColor

        cell.profImg.layer.cornerRadius = cell.profImg.frame.size.width/2
        cell.profImg.clipsToBounds = true
        // Configure the cell...
        
        if indexPath.row <= arr.count {
            cell.pullName.text = arr[indexPath.row*6]["title"] as? String
            cell.pullDesc.text = arr[indexPath.row*6+1]["body"] as? String
            cell.userName.text = arr[indexPath.row*6+2]["login"] as? String
            cell.profImg.sd_setImage(with: URL(string: (arr[indexPath.row*6+3]["avatar_url"] as? String)!), placeholderImage: #imageLiteral(resourceName: "octocat"), options: [.continueInBackground, .progressiveDownload])
            cell.datePull.text = (arr[indexPath.row*6+4]["pushed_at"] as? String)?.replacingOccurrences(of: "Z", with: "")
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.alpha = 0
        
        let transform = CATransform3DTranslate(CATransform3DIdentity, -(UIScreen.main.bounds.width), 20, 0)
        cell.layer.transform = transform
        
        UIView.animate(withDuration: 1.0) {
            cell.alpha = 1
            cell.layer.transform = CATransform3DIdentity
        }
        
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let indexPath = tableView.indexPathForSelectedRow
        repo.link = arr[(indexPath?.row)!*6+5]["html_url"]!
    }
}


