//
//  PullTableViewCell.swift
//  DesafioConcrete
//
//  Created by Victor Hugo Martins Lisboa on 29/11/2017.
//  Copyright © 2017 Victor Hugo Martins Lisboa. All rights reserved.
//

import UIKit

class PullTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var pullName: UILabel!
    @IBOutlet weak var pullDesc: UILabel!
    @IBOutlet weak var profImg: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var datePull: UILabel!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
