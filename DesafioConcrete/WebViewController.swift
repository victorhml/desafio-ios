//
//  WebViewController.swift
//  DesafioConcrete
//
//  Created by Victor Hugo Martins Lisboa on 03/12/2017.
//  Copyright © 2017 Victor Hugo Martins Lisboa. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {


    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = repo.link.replacingOccurrences(of: "https://github.com/ReactiveX/RxJava/pull/", with: "#")
        let url = URL(string: repo.link)
        let request = URLRequest(url: url!)
        
        webView.load(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
